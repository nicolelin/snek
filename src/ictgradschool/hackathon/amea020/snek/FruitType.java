package ictgradschool.hackathon.amea020.snek;

/**
 * Represents different kinds of fruit which may spawn. Different kinds of fruit may have different graphics,
 * point values, and growth amounts.
 *
 * For a description of the enum syntax used here, see the Direction enum.
 */
public enum FruitType {
    Apple(0, 3),
    Strawberry (1, 2),
    Orange (1, 3);

    /** The position of this fruit's graphics in the tileset. */
    private final int tileX, tileY;

    /** The number of points this fruit is worth. */
    private final int points;

    /** The amount this fruit makes a snek grow when it's eaten. */
    private final int growAmount;

    /**
     * Creates a new fruit with the default number of points and growth.
     *
     * @param tileX The x position of this fruit's graphics in the tileset.
     * @param tileY The y position of this fruit's graphics in the tileset.
     */
    FruitType(int tileX, int tileY) {
        this(tileX, tileY, 100, 3);
    }

    /**
     * Creates a new fruit with the given points and growth.
     *
     * @param tileX The x position of this fruit's graphics in the tileset.
     * @param tileY The y position of this fruit's graphics in the tileset.
     * @param points The number of points this fruit is worth.
     * @param growAmount The amount this fruit makes a snek grow when it's eaten.
     */
    FruitType(int tileX, int tileY, int points, int growAmount) {
        this.tileX = tileX;
        this.tileY = tileY;
        this.points = points;
        this.growAmount = growAmount;
    }

    /** Gets the x position of this fruit's graphics in the tileset. */
    public int getTileX() {
        return tileX;
    }

    /** Gets the y position of this fruit's graphics in the tileset. */
    public int getTileY() {
        return tileY;
    }

    /** Gets the number of points the fruit is worth */
    public int getPoints() {
        return points;
    }

    /** Gets the amount this fruit makes a snek grow when it's eaten. */
    public int getGrowAmount() {
        return growAmount;
    }
}
